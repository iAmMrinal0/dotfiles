# dotfiles


The configs for the following programs are in this repo:

 - Compton
 - Emacs
 - i3 Window Manager
 - MPD
 - NCMPCPP
 - Terminator
 - ZSH

### License
 See the [LICENSE](LICENSE) file for license rights and limitations (MIT)
